package com.chase.schools.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.chase.schools.R
import com.chase.schools.databinding.FragmentMainSchoolsBinding
import com.chase.schools.model.data.School
import com.chase.schools.view.adapter.OnItemClickListener
import com.chase.schools.view.adapter.SchoolsAdapter
import com.chase.schools.viewmodel.SchoolsViewModel

class MainSchoolsFragment : Fragment(), OnItemClickListener {

    private lateinit var schoolsViewModel: SchoolsViewModel
    private lateinit var schoolsAdapter: SchoolsAdapter

    private var _binding: FragmentMainSchoolsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolsViewModel = ViewModelProvider(this)[SchoolsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainSchoolsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressBar.visibility = View.VISIBLE
        binding.schoolsList.layoutManager = LinearLayoutManager(binding.root.context)
        schoolsAdapter = SchoolsAdapter(this)
        binding.schoolsList.adapter = schoolsAdapter

        schoolsViewModel.schoolsLiveData.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            schoolsAdapter.setSchools(it)
        }
        schoolsViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(this.activity, it.message(), Toast.LENGTH_LONG).show()
        }
        schoolsViewModel.getSchools()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemClicked(school: School) {
        Log.i("School DNB ", school.dbn + "  -  " + school.school_name)
        val bundle = bundleOf("dbnClicked" to school.dbn)
        view?.let { Navigation.findNavController(it) }
            ?.navigate(R.id.from_schoollistfragment_to_schooldetailsfragment, bundle)
    }
}