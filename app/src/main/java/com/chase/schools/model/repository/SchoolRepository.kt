package com.chase.schools.model.repository

import com.chase.schools.model.api.APIClient.schoolsService
import com.chase.schools.model.data.School
import com.chase.schools.model.data.SchoolDetails
import retrofit2.Response

class SchoolRepository {
    suspend fun getSchools(): Response<ArrayList<School>> {
        return schoolsService.getSchools()
    }

    suspend fun getSchoolDetails(dbnClicked: String): Response<Array<SchoolDetails>> {
        return schoolsService.getSchoolDetails(dbnClicked)
    }

}