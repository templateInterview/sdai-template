package com.chase.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chase.schools.model.data.School
import com.chase.schools.model.repository.SchoolRepository
import kotlinx.coroutines.launch
import okhttp3.Response

class SchoolsViewModel(
    private val schoolsRepository: SchoolRepository = SchoolRepository()
) : ViewModel() {
    var schoolsLiveData = MutableLiveData<ArrayList<School>>()
    var errorLiveDate = MutableLiveData<Response>()

    fun getSchools() {
        viewModelScope.launch {
            // TODO 1: Call the API to get the list of schools
            // TODO 2: If the response is successful, post the list of schools to the schoolsLiveData
            // TODO 3: If the response is not successful, post the raw response to the errorLiveData
        }
    }
}
