package com.chase.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chase.schools.model.data.SchoolDetails
import com.chase.schools.model.repository.SchoolRepository
import kotlinx.coroutines.launch

class SchoolDetailsViewModel(
    private val schoolsRepository: SchoolRepository = SchoolRepository()
) : ViewModel() {
    var schoolDetailsLiveData = MutableLiveData<SchoolDetails?>()

    fun getSchoolDetails(dbnClicked: String) {
        viewModelScope.launch {
            // TODO 1: Call the API to get the school details
            // TODO 2: If the response is successful, post the school details to the schoolDetailsLiveData
            // TODO 3: If the response is not successful, post null to the schoolDetailsLiveData
            // TODO IMPORTANT: The API returns an array of SchoolDetails, but we only want the first item in the array
        }
    }
}